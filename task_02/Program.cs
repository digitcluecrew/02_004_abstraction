﻿using System;

namespace task_02
{
    abstract class AbstractHandler
    {
        public virtual void Open()
        {
            Console.WriteLine("Open");
        }
        public virtual void Create()
        {
            Console.WriteLine("Create");
        }
        public virtual void Change()
        {
            Console.WriteLine("Change");
        }
        public virtual void Save()
        {
            Console.WriteLine("Save");
        }
    }

    class XMLHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Open XML");
        }
        public override void Create()
        {
            Console.WriteLine("Create XML");
        }
        public override void Change()
        {
            Console.WriteLine("Change XML");
        }
        public override void Save()
        {
            Console.WriteLine("Save XML");
        }
    }

    class TXTHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Open TXT");
        }
        public override void Create()
        {
            Console.WriteLine("Create TXT");
        }
        public override void Change()
        {
            Console.WriteLine("Change TXT");
        }
        public override void Save()
        {
            Console.WriteLine("Save TXT");
        }
    }

    class DOCHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("Open DOC");
        }
        public override void Create()
        {
            Console.WriteLine("Create DOC");
        }
        public override void Change()
        {
            Console.WriteLine("Change DOC");
        }
        public override void Save()
        {
            Console.WriteLine("Save DOC");
        }
    }

    class MainClass
    {
        public static string GetFileName()
        {
            Console.WriteLine("Select file (just write the file name or q for exit)");
            string input = Console.ReadLine();

            if (String.IsNullOrEmpty(input))
            {
                Console.WriteLine("No file here. Try again");
                return GetFileName();
            }

            if (input == "q")
            {
                return null;
            }

            return input;
        }

        public static string GetFileExtension(string filename)
        {
            int index = filename.LastIndexOf('.');

            if (index < 0) return null;

            return filename.Substring(index + 1);
        }

        public static void HandleFile(AbstractHandler d){
            while(true){
				Console.WriteLine("What would you like to do with file now?");
                string key = Console.ReadLine().ToLower();

                switch (key){
                    case "open":
                        d.Open();
                        break;
                    case "create":
                        d.Create();
                        break;
                    case "change" :
                        d.Change();
                        break;
                    case "save" :
                        d.Save();
                        break;
                    case "q":
                        return;
                }
            }
        }

        public static void Main(string[] args)
        {
            string filename = GetFileName();

            if (String.IsNullOrEmpty(filename)) {
                Console.WriteLine("Bye! Bye!");
                return;
            };

            string ext = GetFileExtension(filename);

            if (String.IsNullOrEmpty(ext)) {
                Console.WriteLine("File has no extension! Bye!");
                return;
            };

            AbstractHandler doc;

            switch (ext) {
                case "xml":
					doc = new XMLHandler();
                    break;
				case "doc":
				case "docx":
                    doc = new DOCHandler();
					break;
				case "txt":
					doc = new TXTHandler();
					break;
                default:
                    Console.WriteLine("Can`t handle file. Bye!");
                    return;
            }

            HandleFile(doc);
        }
    }
}
