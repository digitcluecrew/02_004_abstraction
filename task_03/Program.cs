﻿using System;

namespace task_03
{
    public interface IPlayable
    {
        void Play();
        void Pause();
        void Stop();
    }

    public interface IRecodable
    {
        void Record();
        void Pause();
        void Stop();
    }

    public class Player : IPlayable, IRecodable
    {
        public void Play()
        {
            Console.WriteLine("Play");
        }
        public void Pause()
        {
            Console.WriteLine("Pause");
        }
        public void Stop()
        {
            Console.WriteLine("Stop");
        }

        public void Record()
        {
            Console.WriteLine("Record");
        }
    }

    class MainClass
    {
        public static void Main(string[] args)
        {
            Player player = new Player();

            player.Play();
            player.Pause();
            player.Record();
            player.Stop();

            Console.ReadKey();
        }
    }
}
